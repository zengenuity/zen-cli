<?php

require_once __DIR__ . '/vendor/autoload.php';

use Zengenuity\Ops\Console\Command\CronCommand;
use Zengenuity\Ops\Console\Command\UpdateDrupal7ContribCommand;
use Zengenuity\Ops\Console\Command\DeployDrupal7Command;
use Zengenuity\Ops\Console\Command\DomainExpirationCheckCommand;
use Zengenuity\Ops\Console\Command\SSLExpirationCheckCommand;
use Symfony\Component\Yaml\Yaml;

$console = new \Symfony\Component\Console\Application();

$console->add(new CronCommand());
$console->add(new UpdateDrupal7ContribCommand());
$console->add(new DeployDrupal7Command());
$console->add(new DomainExpirationCheckCommand());
$console->add(new SSLExpirationCheckCommand());
$console->run();
