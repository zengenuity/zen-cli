<?php
namespace Zengenuity\Ops\Console\Command;


use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CronCommand extends Command {


  protected function configure() {
    $this->setName('drupal:cron')
      ->setDescription('Run Drupal cron job')
      ->addOption(
        'hosting-type',
        't',
        InputOption::VALUE_REQUIRED,
        'Hosting Type (standard, pantheon, platform)'
      )
      ->addOption(
        'env',
        'e',
        InputOption::VALUE_OPTIONAL,
        'Environment (for Pantheon hosting types)'
      )
      ->addOption(
        'token',
        'k',
        InputOption::VALUE_OPTIONAL,
        'Security Token (for Pantheon hosting types)'
      )
      ->addOption(
        'cron-url',
        'u',
        InputOption::VALUE_OPTIONAL,
        'Cron URL (for standard hosting type)'
      )
      ->addOption(
        'ssh-address',
        's',
        InputOption::VALUE_OPTIONAL,
        'SSH Address (for Platform.sh hosting type)'
      )
      ->addOption(
        'ssh-key',
        'i',
        InputOption::VALUE_OPTIONAL,
        'SSH Key (for Platform.sh hosting type)'
      );
  }

  /**
   * @param \Symfony\Component\Console\Input\InputInterface $input
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *
   * @return int|null
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    switch ($input->getOption('hosting-type')) {
      case 'pantheon':
        return $this->executePantheon($input, $output);
        break;
        
      case 'standard':
        return $this->executeStandard($input, $output);
        break;
        
      case 'platform':
        return $this->executePlatform($input, $output);
        break;
        
      default:
        $output->writeln('Hosting type is unknown.');
        
    }
    return 1;
  }

  /**
   * @param \Symfony\Component\Console\Input\InputInterface $input
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *
   * @return int
   */
  protected function executePantheon(InputInterface $input, OutputInterface $output) {
    $token = $input->getOption('token');
    $env = $input->getOption('env');
    if (empty($token)) {
      $output->writeln('Pantheon cron requires --token (-k) option.');
    }
    if (empty($env)) {
      $output->writeln('Pantheon cron requires --env (-e) option.');
    }
    if (empty($env) || empty($token)) {
      return 1;
    }
    passthru(__DIR__ . '/../../../vendor/bin/terminus --no-ansi auth:login --machine-token=' . $token);
    passthru(__DIR__ . '/../../../vendor/bin/terminus --no-ansi -y -n drush ' . $env . ' -- core-cron -y');
    return 0;
  }

  /**
   * @param \Symfony\Component\Console\Input\InputInterface $input
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *
   * @return int
   */
  protected function executeStandard(InputInterface $input, OutputInterface $output) {
    $url = $input->getOption('cron-url');
    
    if (empty($url)) {
      $output->writeln('Standard cron requires --cron-url (-u) option.');
      return 1;
    }
    $client = new Client();
    $res = $client->request('GET', $url);
    return ($res->getStatusCode() === 200);
  }

  /**
   * @param \Symfony\Component\Console\Input\InputInterface $input
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *
   * @return int
   */
  protected function executePlatform(InputInterface $input, OutputInterface $output) {
    $ssh_address = $input->getOption('ssh-address');
    $ssh_key = $input->getOption('ssh-key');
    if (empty($ssh_address)) {
      $output->writeln('Platform.sh cron requires --ssh-address (-s) option.');
      return 1;
    }
    if (!empty($ssh_key)) {
      passthru('ssh -i ' . $ssh_key . ' -t ' . $ssh_address . ' drush core-cron');
    }
    else {
      passthru('ssh -t ' . $ssh_address . ' drush core-cron');
    }
    return 0;
  }
}
