<?php
namespace Zengenuity\Ops\Console\Command;

use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DeployDrupal7Command extends Command {
  
  use PantheonCommandsTrait;

  /**
   * UpdateDrupal7Contrib constructor.
   */
  public function __construct() {
    parent::__construct();
    $this->completedUpdates = [];
  }


  protected function configure() {
    $this->setName('drupal:7:deploy')
      ->setDescription('Deploy Drupal 7 to Production')
      ->addOption(
        'hosting-type',
        't',
        InputOption::VALUE_REQUIRED,
        'Hosting Type (standard, pantheon, platform)'
      )
      ->addOption(
        'env',
        'e',
        InputOption::VALUE_OPTIONAL,
        'Environment (for Pantheon and Platform.sh hosting types)'
      )
      ->addOption(
        'token',
        'k',
        InputOption::VALUE_OPTIONAL,
        'Security Token (for Pantheon and Platform.sh hosting types)'
      )
      ->addOption(
        'message',
        'm',
        InputOption::VALUE_OPTIONAL,
        'Message for deploy logs',
        ''
      )
      ->addOption(
        'delete-branch',
        'd',
        InputOption::VALUE_NONE,
        'Delete source branch'
      )
      ->addOption(
        'ssh-address',
        's',
        InputOption::VALUE_OPTIONAL,
        'SSH Address (for Platform.sh hosting type)'
      )
      ->addOption(
        'output-report',
        'r',
        InputOption::VALUE_OPTIONAL,
        'Output report file name'
      );
  }

  /**
   * @param \Symfony\Component\Console\Input\InputInterface $input
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *
   * @return int|null
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $this->outputReportFilename = $input->getOption('output-report');
    
    switch ($input->getOption('hosting-type')) {
      case 'pantheon':
        return $this->executePantheon($input, $output);
        break;
        
      case 'standard':
        return $this->executeStandard($input, $output);
        break;
        
      case 'platform':
        return $this->executePlatform($input, $output);
        break;
        
      default:
        $output->writeln('Hosting type is unknown.');
        
    }
    return 1;
  }

  /**
   * @param \Symfony\Component\Console\Input\InputInterface $input
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *
   * @return int
   */
  protected function executePantheon(InputInterface $input, OutputInterface $output) {
    $token = $input->getOption('token');
    $env = $input->getOption('env');
    $note = $input->getOption('message');
    $delete_branch = (bool) $input->getOption('delete-branch');
    
    if (empty($token)) {
      $output->writeln('Pantheon requires --token (-k) option.');
    }
    if (empty($env)) {
      $output->writeln('Pantheon requires --env (-e) option.');
    }
    if (empty($env) || empty($token)) {
      return 1;
    }
    
    $branch_name = $this->pantheonGetBranchName($env);
    
    $this->pantheonLogin($token);
    $site_name = $this->pantheonGetSiteName($env);
    $live_env = $site_name . '.live';
    $test_env = $site_name . '.test';

    $multidevs = $this->pantheonGetMultidevEnvironments($site_name);
    
    $to_merge = [];
    foreach ($multidevs as $multidev) {
      if ($multidev == $branch_name) {
        $to_merge[] = $site_name . '.' . $multidev;
      }
      elseif (strstr($branch_name, '*')) {
        $regex = '/^' . str_replace('*', '.*', $branch_name) . '$/';
        if (preg_match($regex, $multidev)) {
          $to_merge[] = $site_name . '.' . $multidev;;
        }
      }
    }

    foreach ($to_merge as $multidev) {
      $this->pantheonMergeBranchToDev($multidev, TRUE);
    }

    if (!empty($to_merge) || $branch_name == 'dev') {
      $this->pantheonDeployToEnvironment($test_env, $note, TRUE, TRUE);
      sleep(30);
    }
    if (!empty($to_merge) || $branch_name == 'dev' || $branch_name == 'test') {
      $this->pantheonDeployToEnvironment($live_env, $note, TRUE, TRUE);
      sleep(30);
      $this->pantheonClearDrupal7Cache($live_env);
      sleep(30);
      $this->pantheonClearEnvironmentCache($live_env);
    }

    if ($delete_branch) {
      foreach ($to_merge as $multidev_env) {
        $this->pantheonDeleteMultidev($multidev_env, TRUE);
      }
    }

    if (!empty($output_report) && !empty($to_merge) && $fh = fopen($output_report, 'w')) {
      fwrite($fh, "Deployed website to production.\n\n");
      $login_url = $this->pantheonGetAdminLoginUrl($live_env);
      fwrite($fh, 'Login: ' . $login_url . "\n\n");
      fclose($fh);
    }
    
    return 0;
  }

  /**
   * @param \Symfony\Component\Console\Input\InputInterface $input
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *
   * @return int
   */
  protected function executeStandard(InputInterface $input, OutputInterface $output) {
    $url = $input->getOption('cron-url');
    
    if (empty($url)) {
      $output->writeln('Standard cron requires --cron-url (-u) option.');
      return 1;
    }
    $client = new Client();
    $res = $client->request('GET', $url);
    return ($res->getStatusCode() === 200);
  }

  /**
   * @param \Symfony\Component\Console\Input\InputInterface $input
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *
   * @return int
   */
  protected function executePlatform(InputInterface $input, OutputInterface $output) {
    $ssh_address = $input->getOption('ssh-address');
    if (empty($ssh_address)) {
      $output->writeln('Platform.sh cron requires --ssh-address (-s) option.');
      return 1;
    }
    passthru('ssh -t ' . $ssh_address . ' drush core-cron');
    return 0;
  }
}
