<?php

namespace Zengenuity\Ops\Console\Command;


trait PantheonCommandsTrait {

  protected $terminus = __DIR__ . '/../../../vendor/bin/terminus';
  /**
   * @param string $token
   */
  protected function pantheonLogin(string $token) {
    passthru($this->terminus . ' --no-ansi auth:login --machine-token=' . $token);
  }

  /**
   * @param string $environment
   * @param string $command
   * @param array $output
   *
   * @return string
   */
  protected function pantheonDrush(string $environment, string $command, array &$output = []) : string {
    return exec($this->terminus . ' remote:drush ' . $environment . ' -- ' . $command, $output);
  }

  /**
   * @param string $environment
   * @param string $command
   * @param array $output
   *
   * @return string
   */
  protected function pantheonWpCli(string $environment, string $command, array &$output = []) : string {
    return exec($this->terminus . ' remote:wp ' . $environment . ' -- ' . $command, $output);
  }

  /**
   * @param string $environment
   */
  protected function pantheonClearDrupal7Cache(string $environment) {
    $this->pantheonDrush($environment, 'cc all');
  }

  /**
   * @param string $environment
   */
  protected function pantheonClearEnvironmentCache(string $environment) {
    exec($this->terminus . ' env:cc ' . $environment);
  }

  /**
   * @param string $branch_name
   * @param bool $delete_branch
   */
  protected function pantheonDeleteMultidev(string $branch_name, bool $delete_branch = TRUE) {
    $command = $this->terminus . ' multidev:delete -y';
    if ($delete_branch) {
      $command .= ' --delete-branch';
    }
    $command .= ' ' . $branch_name;
    exec($command);
  }

  /**
   * @param string $environment
   * @param bool $security_only
   *
   * @return array
   */
  protected function pantheonGetAvailableDrupalUpdates(string $environment, bool $security_only) : array {
    $projects = [];
    $this->pantheonDrush($environment, 'cc all');
    if ($security_only) {
      $this->pantheonDrush($environment, 'pm-updatestatus --pipe --security-only', $projects);
    }
    else {
      $this->pantheonDrush($environment, 'pm-updatestatus --pipe', $projects);
    }
    // Clean up SSH messages
    foreach ($projects as $i => $project) {
      if (preg_match('/[^0-9A-z\_\-]/', $project) || $project == 'drupal' || empty($project)) {
        unset($projects[$i]);
      }
      if (strstr($project, 'Warning: Permanently') || strstr($project, 'No release history was found') || $project == 'drupal' || empty($project)) {
        unset($projects[$i]);
      }
    }
    return $projects;
  }

  /**
   * @param string $environment
   * @param bool $security_only
   *
   * @return array
   */
  protected function pantheonGetAvailableWpUpdates(string $environment, bool $security_only) : array {
    $projects = [];
    $output = [];
    $this->pantheonWpCli($environment, 'plugin list --status=active --update=available --format=json', $output);
    if (!empty($output[0])) {
      $projects = json_decode($output[0], TRUE);
    }
    return $projects;
  }

  /**
   * @param string $environment
   * @param string $project
   *
   * @return string
   */
  protected function pantheonGetContribProjectVersion(string $environment, string $project) : string {
    return $this->pantheonDrush($environment, 'pm-info ' . $project . ' --fields=version --format=list');
  }

  /**
   * @param string $environment
   * @param string $project
   *
   * @return string
   */
  protected function pantheonGetWpPluginVersion(string $environment, string $project) : string {
    $json = $this->pantheonWpCli($environment, ' --name="' . $project . '" --format=json plugin list');
    if (!empty($json)) {
      $data = json_decode($json, TRUE);
      if (!empty($data[0]['version'])) {
        return $data[0]['version'];
      }
    }
    return 'unknown';
  }

  /**
   * @param string $environment
   *
   * @return string
   */
  protected function pantheonGetAdminLoginUrl(string $environment) : string {
    return $this->pantheonDrush($environment, 'uli');
  }

  /**
   * @param string $environment
   *
   * @return string
   */
  protected function pantheonGetWpAdminUrl(string $environment) : string {
    $output = [];
    exec($this->terminus . ' env:info ' . $environment . ' --format=json', $output);
    if (!empty($output)) {
      $json = implode('', $output);
      $data = json_decode($json, TRUE);
      return 'https://' . $data['domain'] . '/wp-admin';
    }
    return '';
  }

  /**
   * @param string $environment
   * @param string $message
   */
  protected function pantheonCommitChanges(string $environment, string $message) {
    exec($this->terminus . ' env:commit ' . $environment . ' --message="' . $message . '"');
  }

  /**
   * @param string $environment
   * @param string $project
   */
  protected function pantheonUpdateContribProject(string $environment, string $project) {
    $this->pantheonDrush($environment, 'pm-updatecode  --no-core -y ' . $project);
  }

  /**
   * @param string $environment
   * @param string $project
   */
  protected function pantheonUpdateWpPlugin(string $environment, string $project) {
    $this->pantheonWpCli($environment, '--format=json plugin update "' . $project . '"');
  }

  /**
   * @param string $environment
   *
   * @return bool
   */
  protected function pantheonConfirmChangedFilesExist(string $environment) : bool {
    $check_count = 0;
    $changed_files = [];
    do {
      exec($this->terminus . ' env:diffstat ' . $environment . ' --format=string', $changed_files);
      sleep(10);
      $check_count++;
    } while ($check_count < 10 && empty($changed_files));
    return !empty($changed_files);
  }

  /**
   * @param string $site_name
   *
   * @return array
   */
  protected function pantheonGetMultidevEnvironments(string $site_name) : array {
    $multidevs = [];
    exec($this->terminus . ' multidev:list ' . $site_name . ' --field=id', $multidevs);
    return $multidevs;
  }

  /**
   * @param string $environment
   * @param string $mode
   */
  protected function pantheonSwitchMode(string $environment, string $mode) {
    exec($this->terminus . ' connection:set ' . $environment . ' ' . $mode);
  }

  /**
   * @param string $name
   * @param string $from_environment
   */
  protected function pantheonCreateMultdevEnvironment(string $name, string $from_environment) {
    exec($this->terminus . ' multidev:create ' . $from_environment . ' ' . $name);
  }

  /**
   * @param string $environment
   *
   * @return string
   */
  protected function pantheonGetSiteName(string $environment) : string {
    return preg_replace('/\.[^\.]*$/', '', $environment);
  }

  /**
   * @param string $environment
   *
   * @return string
   */
  protected function pantheonGetBranchName(string $environment) : string {
    $branch_name = '';
    if (preg_match('/\.([^\.]*)$/', $environment, $matches)) {
      $branch_name = $matches[1];
    }
    return $branch_name;
  }

  /**
   * @param string $branch
   * @param bool $update_db
   */
  protected function pantheonMergeBranchToDev(string $branch, bool $update_db = TRUE) {
    $command = $this->terminus . ' multidev:merge-to-dev ' .  $branch . ' -y'; 
    if ($update_db) {
      $command .= ' --updatedb';
    }
    exec($command);
  }

  /**
   * @param string $environment
   * @param string $note
   * @param bool $clear_caches
   * @param bool $update_db
   */
  protected function pantheonDeployToEnvironment(string $environment, string $note = '', bool $clear_caches = TRUE, bool $update_db = TRUE) {
    $command = $this->terminus . ' env:deploy -y';
    if (!empty($note)) {
      $command .= ' --note="' . $note . '"';
    }
    if ($clear_caches) {
      $command .= ' --cc';
    }
    if ($update_db) {
      $command .= ' --updatedb';
    }
    $command .= ' ' . $environment;
    exec($command);
  }

}
