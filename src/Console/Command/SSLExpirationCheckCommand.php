<?php
namespace Zengenuity\Ops\Console\Command;


use GuzzleHttp\Client;
use Novutec\WhoisParser\Parser;
use Spatie\SslCertificate\Exceptions\CouldNotDownloadCertificate;
use Spatie\SslCertificate\SslCertificate;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SSLExpirationCheckCommand extends Command {


  protected function configure() {
    $this->setName('ssl:expire')
      ->setDescription('Check for SSL certification expiration')
      ->addArgument(
        'domain-name',
        InputArgument::REQUIRED,
        'Domain name to check'
      )
      ->addArgument(
        'error-limit',
        InputArgument::OPTIONAL,
        'Limit in days for which an error should be thrown'
      );
  }

  /**
   * @param \Symfony\Component\Console\Input\InputInterface $input
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *
   * @return int|null
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $domain = $input->getArgument('domain-name');
    try {
      $cert = SslCertificate::createForHostName($domain);
      $expire_date = $cert->expirationDate();
      $days_left = $expire_date->diff(new \DateTime())->format('%a');
      $output->writeln('SSL certificate on ' . $domain . ' will expire in ' . $days_left . ' days.');
      $error_limit = $input->getArgument('error-limit');
      if ($error_limit !== null && $error_limit >= $days_left) {
        $output->writeln('Less than limit of ' . $error_limit . ' days.');
        return 1;
      }
      return 0;
    }
    catch (CouldNotDownloadCertificate $e) {
      $output->writeln('Cound not download SSL certificate for ' . $domain . '.');
      return 1;
    }
  }

}
