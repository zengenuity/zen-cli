<?php
namespace Zengenuity\Ops\Console\Command;

use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateWpPluginsCommand extends Command {
  
  use PantheonCommandsTrait;

  /**
   * @var bool
   */
  protected $securityOnly;

  /**
   * @var string
   */
  protected $outputReportFilename;

  /**
   * @var array
   */
  protected $completedUpdates;

  /**
   * UpdateDrupal7Contrib constructor.
   */
  public function __construct() {
    parent::__construct();
    $this->completedUpdates = [];
  }


  protected function configure() {
    $this->setName('wp:update:plugins')
      ->setDescription('Apply Wordpress plugin updates')
      ->addOption(
        'hosting-type',
        't',
        InputOption::VALUE_REQUIRED,
        'Hosting Type (standard, pantheon, platform)'
      )
      ->addOption(
        'env',
        'e',
        InputOption::VALUE_OPTIONAL,
        'Environment (for Pantheon and Platform.sh hosting types)'
      )
      ->addOption(
        'token',
        'k',
        InputOption::VALUE_OPTIONAL,
        'Security Token (for Pantheon and Platform.sh hosting types)'
      )
      ->addOption(
        'ssh-address',
        's',
        InputOption::VALUE_OPTIONAL,
        'SSH Address (for Platform.sh hosting type)'
      )
      ->addOption(
        'security-only',
        'o',
        InputOption::VALUE_NONE,
        'Security updates only'
      )
      ->addOption(
        'output-report',
        'r',
        InputOption::VALUE_OPTIONAL,
        'Output report file name'
      );
  }

  /**
   * @param \Symfony\Component\Console\Input\InputInterface $input
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *
   * @return int|null
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $this->securityOnly = (bool) $input->getOption('security-only');
    $this->outputReportFilename = $input->getOption('output-report');
    
    switch ($input->getOption('hosting-type')) {
      case 'pantheon':
        return $this->executePantheon($input, $output);
        break;
        
      case 'standard':
        return $this->executeStandard($input, $output);
        break;
        
      case 'platform':
        return $this->executePlatform($input, $output);
        break;
        
      default:
        $output->writeln('Hosting type is unknown.');
        
    }
    return 1;
  }

  /**
   * @param \Symfony\Component\Console\Input\InputInterface $input
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *
   * @return int
   */
  protected function executePantheon(InputInterface $input, OutputInterface $output) {
    $token = $input->getOption('token');
    $env = $input->getOption('env');
    if (empty($token)) {
      $output->writeln('Pantheon requires --token (-k) option.');
    }
    if (empty($env)) {
      $output->writeln('Pantheon requires --env (-e) option.');
    }
    if (empty($env) || empty($token)) {
      return 1;
    }
    
    $this->pantheonLogin($token);
    $projects = $this->pantheonGetAvailableWpUpdates($env, $this->securityOnly);
    
    if (!empty($projects)) {
      $branch = 'up' . date('Ymd');
      $site_name = $this->pantheonGetSiteName($env);
      $multidev_env = $site_name . '.' . $branch;
     
      
      // See if environment exists
      $existing_multidevs = $this->pantheonGetMultidevEnvironments($site_name);
      if (!in_array($branch, $existing_multidevs)) {
        $output->writeln("Creating multidev environment: " . $multidev_env);
        $this->pantheonCreateMultdevEnvironment($branch, $env);
      }
      else {
        $output->writeln("Found existing multidev environment: " . $multidev_env);
      }
      
      $output->writeln("Switching to SFTP mode.");
      $this->pantheonSwitchMode($multidev_env, 'sftp');
    }
    else {
      $output->writeln("Nothing to update.");
      return 0;
    }

    // Re-pull projects to update from the multidev env, in case they have already been updated.
    // Always do security updates first
    $projects = $this->pantheonGetAvailableWpUpdates($multidev_env, $this->securityOnly);
    foreach ($projects as $project) {
      $output->writeln("Updating project: " . $project['name']);
      $initial_version = $this->pantheonGetWpPluginVersion($multidev_env, $project['name']);

      $this->pantheonUpdateWpPlugin($multidev_env, $project['name']);
      
      $output->writeln("Waiting for file system to catch up for commit.");
      $this->pantheonConfirmChangedFilesExist($multidev_env);

      $final_version = $this->pantheonGetWpPluginVersion($multidev_env, $project['name']);
      $command_output = [];
      $title = $project['name'];
      $this->pantheonWpCli($multidev_env, 'plugin status "' . $project['name'] . '"', $command_output);
      foreach ($command_output as $line) {
        if (preg_match('/Name: (.*)/', $line, $matches)) {
          $title = $matches[1];
        }
      }
      $output->writeln("Committing.");
      $this->pantheonCommitChanges($multidev_env, 'Plugin update: ' . $title . ': ' . $initial_version . ' to ' . $final_version);
      sleep(30);

      $this->completedUpdates[] = [
        'title' => $title,
        'project' => $project['name'],
        'initial_version' => $initial_version,
        'final_version' => $final_version,
        'security_update' => TRUE,
      ];
    }

    $output->writeln("Switching to git mode.");
    $this->pantheonSwitchMode($multidev_env, 'git');

    if (!empty($this->outputReportFilename) && !empty($this->completedUpdates) && $fh = fopen($this->outputReportFilename, 'w')) {
      $login_url = $this->pantheonGetWpAdminUrl($multidev_env);
      
      fwrite($fh, 'Login: ' . $login_url . "\n\n");
      fwrite($fh, "Updates applied:\n");
      
      foreach ($this->completedUpdates as $update) {
        $output = $update['title'] . ' (' . $update['project'] . '): ' . $update['initial_version'] . ' to ' . $update['final_version'];
        if (!empty($update['security_update'])) {
          $output .= ' (security update)';
        }
        $output .= "\n";
        fwrite($fh, $output);
      }
      fclose($fh);
    }
    
    return 0;
  }

  /**
   * @param \Symfony\Component\Console\Input\InputInterface $input
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *
   * @return int
   */
  protected function executeStandard(InputInterface $input, OutputInterface $output) {
    $url = $input->getOption('cron-url');
    
    if (empty($url)) {
      $output->writeln('Standard cron requires --cron-url (-u) option.');
      return 1;
    }
    $client = new Client();
    $res = $client->request('GET', $url);
    return ($res->getStatusCode() === 200);
  }

  /**
   * @param \Symfony\Component\Console\Input\InputInterface $input
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *
   * @return int
   */
  protected function executePlatform(InputInterface $input, OutputInterface $output) {
    $ssh_address = $input->getOption('ssh-address');
    if (empty($ssh_address)) {
      $output->writeln('Platform.sh cron requires --ssh-address (-s) option.');
      return 1;
    }
    passthru('ssh -t ' . $ssh_address . ' drush core-cron');
    return 0;
  }
}
